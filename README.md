# Bootable squashfs

```
$ tree -L 3
.
├── bin
│   ├── ls
│   ├── mkdir
│   ├── mount
│   └── umount
├── dev
├── etc
├── init
├── lib
│   └── x86_64-linux-gnu
│       ├── libacl.so.1
│       ├── libattr.so.1
│       ├── libblkid.so.1
│       ├── libc.so.6
│       ├── libdl.so.2
│       ├── libmount.so.1
│       ├── libpthread.so.0
│       ├── librt.so.1
│       ├── libselinux.so.1
│       ├── libsepol.so.1
│       └── libuuid.so.1
├── lib64
│   └── ld-linux-x86-64.so.2
├── mnt
│   └── root
├── proc
├── sbin
└── sys
```

```
~# cat /etc/grub.d/40_custom
#!/bin/sh
exec tail -n +3 $0
# This file provides an easy way to add custom menu entries.  Simply type the
# menu entries you want to add after this comment.  Be careful not to change
# the 'exec tail' line above.
menuentry 'Custom initramfs' --class debian --class gnu-linux --class gnu --class os {
		load_video
		insmod gzio
		insmod part_msdos
		insmod ext2
		set root='(/dev/sda,msdos1)'
		# TODO: is that necessary?
		search --no-floppy --fs-uuid --set=root foo-bar-baz
		echo    'Loading Linux ...'
		linux   /vmlinuz-$(uname -r) root=/dev/sda1 ro single vga=792
		echo    'Loading initial ramdisk ...'
		initrd  /custom-initramfs.cpio.gz
}
~# update-grub
```

```
~# cd initramfs
~# find . -print0 | cpio --null -ov --format=newc | pigz -p 16 -9 > /boot/custom-initramfs.cpio.gz
```
